// window.R is Raphael object
// window.T is Tachometer instance

var val = document.getElementById('val'),
    min = document.getElementById('min'),
    max = document.getElementById('max'),
    ss = document.getElementById('ss'),
    se = document.getElementById('se'),
    num = document.getElementById('num'),
	val_out = document.getElementById('val-out'),
    min_out = document.getElementById('min-out'),
    max_out = document.getElementById('max-out'),
    ss_out = document.getElementById('ss-out'),
    se_out = document.getElementById('se-out'),
	opts = {
	  interactive: true,
	  needleAnimation: false
	};
	
setup();

function setup() {
	output(val_out, val);
	output(min_out, min);
	output(max_out, max);
	output(ss_out, ss);
	output(se_out, se);
}

function keycode(e){
    if(document.all) {
        return  e.keyCode;
	}
    else if(document.getElementById) {
        return (e.keyCode)? e.keyCode: e.charCode;
	}
    else if(document.layers) {
        return  e.which;
	}
}

function output(elm, src) {
	elm.innerHTML = parseInt(src.value);
}

function update(opts) {
	var value = T.get();
	
	R.clear();
	T = R.tachometer(value, opts);
}

function changeVal(value, e) {
	if ((e && keycode(e) != 13) || !value) return;
	
	value = parseInt(value);
	
	val.value = value;
	output(val_out, val);
	
	T.set(value, true);
}

function changeMin(value, e) {
	if ((e && keycode(e) != 13) || !value) return;	
	
	value = (value > T.get()) ? T.get() : value;
	
	value = parseInt(value);
	
	min.value = value;
	val.min = value;
	output(min_out, min);
	
	opts.numberMin = value;
	update(opts);
}

function changeMax(value, e) {
	if ((e && keycode(e) != 13) || !value) return;
	
	value = (value < T.get()) ? T.get() : value;
	
	value = parseInt(value);
	
	max.value = value;
	val.max = value;
	output(max_out, max);
	
	opts.numberMax = value;
	update(opts);
}

function changeSS(value, e) {
	if ((e && keycode(e) != 13) || !value) return;
	
	value = parseInt(value);
	
	value = (value > se.value) ? se.value : value;
	
	value = parseInt(value);
	
	ss.value = value;
	output(ss_out, ss);
	
	opts.scaleAngleStart = value;
	update(opts);
}

function changeSE(value, e) {
	if ((e && keycode(e) != 13) || !value) return;
	
	value = parseInt(value);
	
	value = (value < ss.value) ? ss.value : value;
	
	value = parseInt(value);
	
	se.value = value;
	output(se_out, se);
	
	opts.scaleAngleEnd = value;
	update(opts);
}

function changeNum(checked) {
	opts.number = checked;
	
	update(opts);
}